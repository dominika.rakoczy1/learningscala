import cats.kernel.Monoid
import cats.{Apply, Monad, Semigroup}
import cats.implicits._

class HigherLvlOfAbstraction {


  //  1. Napisz funkcję `reduce`, która przyjmuje `List[A]` i zwraca `Option[A]` korzystając z typeclassy `Semigroup`
  //    z biblioteki `cats` (wykorzystaj metodę `combine z` Semigroup`).
  //  2. Napisz funkcję `fold`, która przyjmuje `List[A]` i zwraca `A` korzystając z typeclassy `Monoid` z biblioteki `cats`
  //  (wykorzystaj metody `combine` i `empty` zz `Monoid`).
  //  3. Napisz funkcję `combineF`, która przyjmuje dwa `F[A]` i zwraca jedno `F[A]` korzystając z typeclass `Semigroup` i
  //  `Apply` z biblioteki `cats` (wykorzystaj metody `combine` z `Semigroup` i `product` z `Apply`).

  def reduce[A](a: List[A])(implicit as: Semigroup[A]): Option[A] = {
    a.reduceOption(as.combine) //==
    a.reduceOption((a, b) => as.combine(a, b))
  }

  // ==
  def reduce[A: Semigroup](a: List[A]): Option[A] = {
    Semigroup[A].combineAllOption(a)
  }

  def fold[A](a: List[A])(implicit as: Monoid[A]): A = {
    a.foldLeft(as.empty)(as.combine)
  }

  def combineF[F[_], A](a: F[A], b: F[A])(implicit s: Semigroup[A], app: Apply[F]): F[_] = {
    app.product(a, b).map(c => s.combine(c._1, c._2))

    app.product(a, b).map{case (c,d) => s.combine(c, d)}
  }

}
