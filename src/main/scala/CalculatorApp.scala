import javax.xml.ws.Response

import scala.io.StdIn
import scala.util.{Failure, Success, Try}
import cats.effect._

case class Operation(leftArg: Int, operator: String, rightArg: Int)

object CalculatorApp extends App {

  loop().unsafeRunSync()

  def loop(): IO[Unit] = {
    for {
      _ <- printLine("Type operation")
      input <- readLine()
      operation <- IO.fromTry(parseOperation(input))
      result = calculate(operation)
      _ <- printLine(mapResponseToText(result))
      _ <- loop()
    } yield ()
  }

  def parseOperation(rawOperation: String): Try[Operation] = {
    (rawOperation.split(" ") match {
      case Array(i, j, x) =>
        for {
          i <- toNatural(i: String) // zworci taka monade jak bedzie w pierwszym elemencie
          k <- toNatural(x: String)
        } yield Operation(i, (j: String), k)
      case _ => Failure(new IllegalArgumentException("Try to type operation using spaces beetwen signs"))
    })
  }
  def printLine(value: String): IO[Unit] = IO(println(value))
  def readLine(): IO[String] = IO(StdIn.readLine())
  def mapResponseToText(response: Try[Int]): String = {
    response match {
      case Failure(exception) => exception.getMessage
      case Success(value) => s"Result is: $value "
    }
  }
  def toNatural(value: String): Try[Int] = {
    Try(value.toInt).flatMap { val1 =>
      if (val1 >= 0) Success(val1)
      else Failure(new IllegalArgumentException("Use only natural numbers"))
    }
  }
  def calculate(operation: Operation): Try[Int] = {
    operation match {
      case Operation(a, "+", b) => Success(add(a)(b))
      case Operation(a, "-", b) => subtract(a, b)
      case Operation(a, "/", b) => divide(a, b)
      case Operation(a, "*", b) => Success(multiply(a, b))
      case _ => Failure(new NoSuchMethodError())
    }
  }

  def add(a: Int)(b: Int): Int = a + b

  def subtract(a: Int, b: Int): Try[Int] = {
    if (a < b) Failure(new IllegalArgumentException("Use only natural numbers"))
    else Success(a - b)
  }

  def divide(a: Int, b: Int): Try[Int] = {
    if (b == 0) Failure(new IllegalArgumentException("Dont divide by 0"))
    else if (a % b == 0) Success(a / b)
    else Failure(new IllegalArgumentException("The result is not a natural number"))
  }

  def multiply(a: Int, b: Int): Int = a * b

}