import java.io.{File, IOException, PrintWriter}
import java.nio.file.Files

import cats.effect._
import cats.implicits._
import io.circe.generic.auto._
import org.http4s.EntityDecoder
import org.http4s.circe.CirceEntityDecoder._
import org.http4s.client.blaze.BlazeClientBuilder
import better.files._
import better.files.File._
import scala.concurrent.ExecutionContext.global
import scala.io.StdIn
import scala.util.Try
//log4j 2 implementacja sl4j dodac
//config log4j config

case class Post(userId: Int, id: Int, title: String, body: String)

case class Comment(postId: Int, id: Int, name: String, email: String, body: String)

object JsonReaderApp extends IOApp {

  val httpBasePath: String = "https://jsonplaceholder.typicode.com"

  override def run(args: List[String]): IO[ExitCode] = {
    groupCommentsFromPosts().handleErrorWith(throwable => printLine(throwable.getMessage)).as(ExitCode.Success)
  }

  def readAndWritePosts(): IO[Unit] = {
    for {
      _ <- printLine("Type number of post to save:")
      numberOfPosts <- readLine()
      numberOfPosts <- IO.fromTry(toInt(numberOfPosts))
      posts <- callApi[Post]("/posts")
      _ <- printLine("Type file name to save:")
      fileName <- readLine()
      response <- savePosts(posts, numberOfPosts, fileName)
    } yield response
  }

  def groupCommentsFromPosts(): IO[Unit] = {
    for {
      _ <- printLine("Type number of post's comments to save:")
      numberOfPosts <- readLine()
      numberOfPosts <- IO.fromTry(toInt(numberOfPosts))
      posts <- callApi[Post]("/posts")
      comments <- callApiForPostsComments(posts, numberOfPosts)
      groupedComments <- IO.fromTry(groupCommentsByEmailDomain(comments))
      _ <- printLine("Type file name to save:")
      fileName <- readLine()
      response <- saveComments(groupedComments, fileName)
    } yield response
  }

  def callApi[T](pathEnding: String)(implicit decoder: EntityDecoder[IO, List[T]]): IO[List[T]] = {
    BlazeClientBuilder[IO](global).resource.use { client =>
      client.expect[List[T]](s"$httpBasePath$pathEnding")
    }
  }

  def savePosts(posts: List[Post], numberOfPostToSave: Int, fileName: String): IO[Unit] = IO {
    writeListToFile[Post](posts.take(numberOfPostToSave), fileName)
  }

  def saveComments(groupedComments: Map[String, List[Comment]], folderName: String): IO[Unit] = IO {
    groupedComments
      .foreach(x => writeListToFile(x._2, s"$folderName/${x._1}"))
  }

  def writeListToFile[T](list: List[T], fileName: String) = {
    s"/$fileName.txt"
      .toFile
      .createIfNotExists(false, true)
      .append(listToString(list)) // zrobić ta metode w osobnym watku by nie blokowała watku
  }

  def callApiForPostsComments(posts: List[Post], numberOfPostToSave: Int): IO[List[Comment]] = {
    fs2.Stream.fromIterator[IO, Int](posts.take(numberOfPostToSave).map(i => i.id).iterator)
      .parEvalMap[IO, List[Comment]](maxConcurrent = 10)(id => callApi[Comment](s"/posts/$id/comments"))
      .flatMap(i => fs2.Stream.fromIterator[IO, Comment](i.iterator))
      .compile.toList
  }

  def groupCommentsByEmailDomain(comments: List[Comment]): Try[Map[String, List[Comment]]] = Try {
    comments.groupBy(_.email.split("@")(1))
  }

  def printLine(value: String): IO[Unit] = IO(println(value))

  def readLine(): IO[String] = IO(StdIn.readLine())

  def toInt(value: String): Try[Int] = Try(value.toInt)

  def listToString[T](posts: List[T]): String = posts.mkString(System.lineSeparator())



}

